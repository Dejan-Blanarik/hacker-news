import React, { Component } from 'react';
import { Link } from 'react-router-dom';

//MUI stuff
import {
    AppBar,
    Toolbar,
    Tooltip,
    Button
} from '@material-ui/core';

//MUI icons
import HomeIcon from '@material-ui/icons/Home';
import LinkedInIcon from '@material-ui/icons/LinkedIn';


class Navbar extends Component {

    render() {

        return (


            <AppBar>
                <Toolbar className='nav-container'>
                    <a
                        href="https://news.ycombinator.com/"
                        target='_blank' rel="noopener noreferrer"
                        className='nav-title'
                    >
                        HACKER NEWS
                    </a>

                    <div className="nav-links">
                        <Tooltip title='Home page'>
                            <Button
                                className='nav-btn'
                                component={Link}
                                to='/'
                                color='inherit'
                            >
                                <HomeIcon fontSize='large' />
                            </Button>
                        </Tooltip>

                        <Tooltip title='Visit my LinkedIn profile'>
                            <a
                                href="https://www.linkedin.com/in/dejan-blanarik-0b75601a9/"
                                target="_blank"
                                className='nav-btn'
                                rel="noopener noreferrer"
                            >
                                <LinkedInIcon
                                    fontSize='large'
                                    style={{ fill: 'rgb(255,255,255)' }}
                                />
                            </a>
                        </Tooltip>
                    </div>
                </Toolbar>
            </AppBar>


        )
    }

}

export default Navbar;