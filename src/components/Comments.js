import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';
import axios from 'axios';

//MUI stuff
import { withStyles } from '@material-ui/core';
import Paper from '@material-ui/core/Paper';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';



//dataActions
import { mapTime } from '../actions/dataActions';

//Helpers
import { storyUrl } from '../util/helpers';


const styles = (theme) => ({
    ...theme.spreadThis
});



class Comments extends Component {
    constructor(props) {
        super(props)

        this.state = {
            id: props.id,
            by: props.by,
            text: props.text,
            time: props.time,
            kids: props.hasOwnProperty('kids') ? props.kids : [],
            parent: props.parent,
            type: props.type,
            comments: [],

            expansionPanelOpen: false,
        };

        this.promises = [];
    }


    componentDidMount() {
        if (this.state.kids.length > 0) {
            this.state.kids.forEach(id => this.promises.push(
                axios.get(`${storyUrl}${id}.json`)
            ));

            Promise.all(this.promises).then(res => {
                let comments = [];

                res.forEach(response => {
                    if (
                        response.hasOwnProperty('data')
                        && response.data !== null
                        && !response.data.hasOwnProperty('deleted')
                        && !response.data.hasOwnProperty('dead')) {

                        comments.push(response.data);
                    }
                });

                this.setState({
                    comments: comments
                }, () => this.promises = []);
            })
        }
    }




    render() {
        const { isChild } = this.props;
        const {
            text,
            by,
            comments,
            time,
            type,
            kids,
            expansionPanelOpen
        } = this.state;



        const output = () => {
            return (
                type === 'comment' ? (
                    <Fragment>
                        {(kids.length > 0) ? (
                            <ExpansionPanel expanded={expansionPanelOpen}>

                                <ExpansionPanelSummary
                                    expandIcon={<ExpandMoreIcon />}
                                    aria-controls='panel-content'
                                    id='panel-header'
                                    onClick={() => {
                                        this.setState({
                                            expansionPanelOpen: !expansionPanelOpen
                                        });
                                    }}
                                >
                                    <div className='parent-comment'>
                                        <h4>{by}</h4>

                                        <p
                                            dangerouslySetInnerHTML={{ __html: text }}
                                            className='comment-content'
                                        />

                                        <p className='timestamp'>
                                            {mapTime(time)}
                                        </p>
                                    </div>
                                </ExpansionPanelSummary>

                                <ExpansionPanelDetails>
                                    {comments.length > 0 ? (
                                        <div className={isChild ? 'child-comment' : 'not-child'}>
                                            <div className='child-comment'>
                                                {comments
                                                    .filter(c => !c.hasOwnProperty('dead') && !c.hasOwnProperty('deleted'))
                                                    .sort((a, b) => b.time - a.time)
                                                    .map(comment => <Comments key={`child_${comment.id}`} isChild={true} {...comment} />)}
                                            </div>
                                        </div>
                                    ) : null}
                                </ExpansionPanelDetails>
                            </ExpansionPanel>
                        ) : (
                                <ExpansionPanel expanded={expansionPanelOpen}>
                                    <ExpansionPanelSummary
                                        aria-controls='panel-content'
                                        id='panel-header'

                                    >
                                        <div className='parent-comment'>
                                            <h4>{by}</h4>
                                            <p dangerouslySetInnerHTML={{ __html: text }} className='comment-content' />
                                            <p className='timestamp'>{mapTime(time)}</p>
                                        </div>
                                    </ExpansionPanelSummary>
                                </ExpansionPanel>
                            )}
                    </Fragment>
                ) : (
                        null
                    )
            );
        };
        return (
            <Fragment>
                <div className='box-container'>
                    {isChild ? (
                        <div style={{ width: '100%' }}>{output()}</div>
                    ) : (
                            <Paper className='comment-container'>{output()}</Paper>
                        )}
                </div>


            </Fragment>
        );
    }
}

Comments.propTypes = {
    isChild: PropTypes.bool,
    id: PropTypes.number.isRequired,
    by: PropTypes.string.isRequired,
    kids: PropTypes.array,
    parent: PropTypes.number,
    text: PropTypes.string.isRequired,
    type: PropTypes.string.isRequired,
    time: PropTypes.number.isRequired
}

export default withStyles(styles)(Comments);