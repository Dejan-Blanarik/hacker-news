import React, { Component, Fragment } from 'react';
import { withRouter } from 'react-router-dom';
import PropTypes from 'prop-types';

//Mui stuff
import { withStyles, Typography } from '@material-ui/core';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Button from '@material-ui/core/Button';

//Icons
import ChatIcon from '@material-ui/icons/Chat';

//Images
import storyImage from '../images/top-story.jpg'
import { mapTime } from '../actions/dataActions';

const styles = (theme) => ({
    ...theme.spreadThis,

})

class SingleNews extends Component {

    constructor(props) {
        super(props)

        this.state = {
            id: props.id,
            by: props.by,
            descendants: props.descendants,
            score: props.score,
            title: props.title,
            type: props.type,
            time: props.time,
            url: props.url,
            author: props.by,
            text: props.text,
            kids: props.hasOwnProperty('kids') ? props.kids : [],
        };

        this.promises = [];
    }



    handleClick = e => {
        e.preventDefault();

        const { history, setCommentIds } = this.props;

        setCommentIds(this.state.kids)
            .then(() => {
                history.push('/comments');
            })
    };

    render() {
        const {
            url,
            title,
            score,
            author,
            descendants,
            type,
            id,
            time
        } = this.state;

        if (id === 0) {
            return '';
        }

        let topStory = (type === 'story') ? (
            <Card className='storyCard' >
                <CardMedia
                    image={storyImage}
                    title="Story image"
                    className='storyImage'
                />

                <CardContent className='storyContent'>
                    <Typography>
                        <a
                            href={url}
                            target="_blank"
                            rel="noopener noreferrer"
                            className='storyTitle'
                        >
                            {title}
                        </a>
                    </Typography>

                    <Typography
                        variant="body2"
                        className='authorScores'
                    >
                        <span className='scorePoint'>
                            {score}
                        </span>

                        <span>points by:</span>

                        <span className='authorName'>
                            {author}
                        </span>
                    </Typography>

                    <div className='cardComments'>
                        <Button
                            onClick={this.handleClick}
                            title='See all comments'
                            className='commentButton'
                            disabled={descendants === 0}
                        >
                            <ChatIcon color='primary' />
                        </Button>
                        <span className='commentCount'>{descendants}</span>
                    </div>

                    <p className='newsTime'>{mapTime(time)}</p>

                </CardContent>
            </Card>
        ) : (
                null
            )

        return (
            <Fragment>
                {topStory}
            </Fragment>
        );
    }


}

SingleNews.propTypes = {
    setCommentIds: PropTypes.func.isRequired
};

export default withStyles(styles)(withRouter(SingleNews));