import React, { Component } from 'react';
// import './App.css';
import './styles.scss'
import { BrowserRouter as Router, Route } from 'react-router-dom';


//Util
import themeFile from './util/theme';

//pages
import HomePage from './pages/HomePage';
import CommentsPage from './pages/CommentsPage';



//Mui stuff
import { ThemeProvider as MuiThemeProvider } from '@material-ui/core/styles';
import createMuiTheme from '@material-ui/core/styles/createMuiTheme';

const theme = createMuiTheme(themeFile);

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      commentIds: []
    };
  }

  setCommentIds = (ids = []) => {
    return new Promise(resolve => {
      this.setState({ commentIds: ids }, () => {
        resolve();
      });
    })
  }

  render() {
    return (
      <MuiThemeProvider theme={theme}>
        <Router>
          <Route
            exact path='/'
            render={() =>
              <HomePage setCommentIds={this.setCommentIds} />
            }
          />
          <Route
            exact path='/comments'
            render={() =>
              <CommentsPage commentIds={this.state.commentIds} setCommentIds={this.setCommentIds} />
            }
          />
        </Router>
      </MuiThemeProvider>
    );
  }
}

export default App;
