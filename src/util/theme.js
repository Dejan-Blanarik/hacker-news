export default {
  palette: {
    primary: {
      light: '#33c9dc',
      main: '#00bcd4',
      dark: '#008394',
      contrastText: '#fff'
    },
    secondary: {
      light: '#ff6333',
      main: '#ff3d00',
      dark: '#b22a00',
      contrastText: '#fff'
    }
  },

  spreadThis: {
    typography: {
      useNextVariants: true
    },

    pageContainer: {
      width: '100%',
      maxWidth: '100%',
      paddingTop: '100px',
      position: 'relative',
      boxSizing: 'border-box'
    },
  }
}