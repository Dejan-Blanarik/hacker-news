import React, { Component, Fragment } from 'react';

import axios from 'axios';
import PropTypes from 'prop-types';

//helper constants
import { topStoriesUrl, storyUrl } from '../util/helpers';
import SingleNews from '../components/SingleNews';

//components
import NavBar from '../components/Navbar';


//MUI stuff
import { withStyles, Button } from '@material-ui/core';
import CircularProgress from '@material-ui/core/CircularProgress';

//MUI Icons
import Arrow from '@material-ui/icons/ArrowDownward';


const styles = (theme) => ({
    ...theme.spreadThis,

    progress: {
        position: 'absolute',
        top: '-30%',
        left: '50%',
        transform: 'translate(-150%, 0)',
        transition: 'all 0.5s'
    }

})

class HomePage extends Component {

    constructor(props) {
        super(props);

        this.state = {
            storyIds: [],
            stories: [],
            loading: true,
            count: 20,
        };

        this.promises = [];
    }

    componentDidMount() {
        const callback = () => {
            console.log(this.state.storyIds.length)
            this.state.storyIds
                .slice(this.state.count - 20, this.state.count)
                .forEach(id => {
                    this.promises.push(axios.get(`${storyUrl}${id}.json`));
                });

            Promise.all(this.promises).then(res => {

                let stories = [];

                res.forEach(response => {

                    if (
                        response.hasOwnProperty('data')
                        && response.data !== null
                        && !response.data.hasOwnProperty('deleted')
                        && !response.data.hasOwnProperty('dead')) {

                        stories.push(response.data);
                    }
                });

                this.setState({
                    stories: stories,
                    loading: false
                }, () => this.promises = []);
            });
        };

        axios
            .get(topStoriesUrl)
            .then((res) => {
                this.setState({
                    storyIds: res.data.sort((a, b) => b - a)
                }, callback);
            })
            .catch((err) => {
                console.log(err);
            })
    }



    handleClick = () => {
        const callback = () => {
            this.state.storyIds
                .slice(this.state.count - 20, this.state.count)
                .forEach(id => {
                    this.promises.push(axios.get(`${storyUrl}${id}.json`));
                });

            Promise.all(this.promises).then(res => {

                let stories = [];

                res.forEach(response => {
                    if (
                        response.hasOwnProperty('data')
                        && response.data !== null
                        && !response.data.hasOwnProperty('deleted')
                        && !response.data.hasOwnProperty('dead')) {

                        stories.push(response.data);
                    }
                });

                this.setState(state => ({
                    stories: state.stories.concat(stories)
                }), () => this.promises = [])
            });
        }

        this.setState(state => ({
            count: state.count + 20
        }), callback)
    }


    render() {
        const { stories, storyIds, loading, count } = this.state;
        const { classes, setCommentIds } = this.props;

        let storyList = loading ? (
            <CircularProgress color='primary' className={classes.progress} size={60} />
        ) : (
                stories
                    .map((story) => <SingleNews key={story.id} {...story} setCommentIds={setCommentIds} />)
            )


        return (
            <Fragment>
                <NavBar />
                <section className={classes.pageContainer}>
                    <div className='container'>

                        {storyList}

                        <div className='centerBtn'>
                            {loading || count >= storyIds.length ? (
                                null
                            ) : (
                                    <Button
                                        color='secondary'
                                        onClick={this.handleClick}
                                        className='loadMoreBtn'
                                    >
                                        Load more stories<Arrow className='arrowDown' fontSize='small' />
                                    </Button>
                                )}
                        </div>
                    </div>
                </section>
            </Fragment>
        );
    }


}

HomePage.propTypes = {
    classes: PropTypes.object.isRequired,
    setCommentIds: PropTypes.func.isRequired
}

export default withStyles(styles)(HomePage);




