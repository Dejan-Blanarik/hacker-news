import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';
import axios from 'axios';
import { storyUrl } from '../util/helpers';

//Components
import Navbar from '../components/Navbar';
import Comments from '../components/Comments';

//MUI stuff
import { withStyles } from '@material-ui/core';
import Button from '@material-ui/core/Button';
import CircularProgress from '@material-ui/core/CircularProgress';


//MUI icons 
import ArrowBackIcon from '@material-ui/icons/ArrowBack';



const styles = (theme) => ({
    ...theme.spreadThis,

    progressLoading: {
        position: 'absolute',
        top: '-30%',
        left: '50%',
        transform: 'translate(-150%, 0)',
        transition: 'all 0.5s'
    },

    arrowLeft: {
        marginRight: '8px'
    }
});

class CommentsPage extends Component {
    constructor(props) {
        super(props)

        this.state = {
            commentIds: props.commentIds,
            comments: [],
            loading: true
        };

        this.promises = [];

        window.addEventListener('load', this.handlePageLoad);
    }

    componentDidMount() {
        this.state.commentIds.forEach(id => {
            this.promises.push(axios.get(`${storyUrl}${id}.json`));

        });

        Promise.all(this.promises).then(res => {
            let comments = [];

            res.forEach(response => {

                if (
                    response.hasOwnProperty('data')
                    && response.data !== null
                    && !response.data.hasOwnProperty('deleted')
                    && !response.data.hasOwnProperty('dead')) {

                    comments.push(response.data);
                }
            });

            this.setState({
                comments: comments,
                loading: false
            }, () => this.promises = []);
        })
            .catch((err) => {
                console.log(err);
            });
    };

    static getDerivedStateFromProps(nextProp, prevState) {
        if (nextProp.commentIds.length !== prevState.commentIds.length)
            return { commentIds: nextProp.commentIds };

        return null;
    }

    handlePageLoad = e => {
        if (this.state.commentIds.length === 0) {
            this.props.history.push('/')
        }
    };

    handleClick = (e) => {
        e.preventDefault();
        this.props.setCommentIds()
            .then(() => {
                this.props.history.push('/');
            })
    }

    render() {
        const { classes } = this.props;
        const { comments, loading } = this.state;

        let commentList =
            comments
                .sort((a, b) => b.time - a.time)
                .map((comment) => <Comments key={comment.id} {...comment} />)

        return (
            <Fragment>
                <Navbar />
                <section className={classes.pageContainer}>
                    {loading ? (
                        <div className="container">
                            <CircularProgress color='primary' className={classes.progressLoading} size={60} />
                        </div>
                    ) : (
                            <div className="container">
                                {commentList}

                                <Button
                                    color='secondary'
                                    onClick={this.handleClick}
                                    className='loadMoreBtn'
                                >
                                    <ArrowBackIcon size='small' className={classes.arrowLeft} />
                                     Go Back
                                </Button>

                            </div>
                        )}
                </section>
            </Fragment>
        );

    }
}

CommentsPage.propTypes = {
    classes: PropTypes.object.isRequired,
    setCommentIds: PropTypes.func.isRequired,
    commentIds: PropTypes.array.isRequired
}

export default withStyles(styles)(withRouter(CommentsPage));